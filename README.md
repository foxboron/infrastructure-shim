<pre>
# lxc launch images:archlinux/current archmanweb
# lxc file push --gid 0 --uid 0 -p ./id_rsa.pub archmanweb/root/.ssh/authorized_keys
# lxc exec archmanweb pacman -Sy python openssh
# lxc exec archmanweb pacman -Rdd man-db
# lxc exec archmanweb systemctl enable --now sshd
# ansible-playbook man.archlinux.org
</pre>


Also things needs to be commented adhoc
<pre>
diff --git a/roles/archmanweb/defaults/main.yml b/roles/archmanweb/defaults/main.yml
index 0bcfdacd..ce501166 100644
--- a/roles/archmanweb/defaults/main.yml
+++ b/roles/archmanweb/defaults/main.yml
@@ -1,7 +1,7 @@
 ---
 archmanweb_dir: '/srv/http/archmanweb'
 archmanweb_cache_dir: '{{ archmanweb_dir }}/cache'
-archmanweb_domain: 'man.archlinux.org'
+  #archmanweb_domain: 'man.archlinux.org'
 archmanweb_allowed_hosts: ["{{ archmanweb_domain }}"]
 archmanweb_nginx_conf: '/etc/nginx/nginx.d/archmanweb.conf'
 archmanweb_repository: 'https://gitlab.archlinux.org/archlinux/archmanweb.git'
</pre>
